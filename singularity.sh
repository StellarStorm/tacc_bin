#!/bin/bash
module load git
module load cuda/11.3
nvidia-modprobe -u -c=0
module load tacc-singularity/3.7.2
# singularity exec --nv -B /work/07300/sgay/maverick2/data:/data/,/work/07300/sgay/maverick2/logs:/logs/,/work/07300/sgay/maverick2/Wellcome:/code/ \
#     /home1/07300/sgay/singularity_images/tf_1.15.2-gpu-py3.sif \
#     /bin/bash

singularity exec --nv -B $WORK/LondonHN/:/code,$WORK/data/:/data/,$WORK/logs:/logs/,$WORK/weights/:/weights,$WORK/predictions/:/predictions,$WORK/json_models/:/models \
    $WORK/singularity_images/tf-2.7.3-gpu-v2.sif \
    /bin/bash

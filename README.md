# TACC bin

These are (hopefully) helpful scripts for TACC (Maverick2 system) that I use
for checking my current jobs, starting interactive sessions, and other
similar tasks.

## Installing
From TACC, run the following to clone this repo:

```bash
git clone https://gitlab.com/StellarStorm/tacc_bin.git $HOME/bin
```

This is enough to access/use these scripts. However, I've also included my
`.bashrc` file, which I've modified to make it easier to run these scripts, as
well as automatically load python3 and git among other improvements. I'm biased
but I suggest you use it as well!


```bash
cp $HOME/.bashrc $HOME/.bashrc.original  # Back up your original config
cp $HOME/bin/.bashrc $HOME/.bashrc
source $HOME/.bashrc
```

Finally, I suggest making these scripts executable so you don't have to run
them through `sh`:
```bash
chmod +x $HOME/bin/*.sh
```

You should now be able to use any of these scripts without having to provide
the full path, from any file system in TACC.

## Descriptions
(This assumes you updated your `.bashrc` and made scripts executable as above.
If not, then make the appropriate corrections such as providing the absolute
file path when you call these scripts.)

1. Check your remaining balance (also shown every day at login)
```bash
$ balance.sh
----------------------- Project balances for user <redacted> -------------------
| Name           Avail SUs     Expires |                                      |
| <REDACTED>          1333  2021-09-30 |                                      |
-------------------------- Disk quotas for user <redacted> ---------------------
| Disk         Usage (GB)     Limit    %Used   File Usage       Limit   %Used |
| /home1              2.5      10.0    24.72        36468      750100    4.86 |
| /work             493.1    1024.0    48.15       163510     3000000    5.45 |
-------------------------------------------------------------------------------
```

2. Start an interactive session:

    a. GTX queue: `gtx_interactive.sh`

    b. V100 queue: `v100_interactive.sh`

    c. P100 queue: `p100_interactive.sh`

3. Start a singularity instance in an interactive session
```bash
singularity.sh
```

*Note:* you may need to edit `singularity.sh` based upon how you have set up
your project directories, and the name of your singularity .sif file.


4. Check the logging output of a running job
```bash
check JOBID
```
where JOBID is the 6-digit ID for your job.

If you're not sure what your job ID is, you can check with `showq -u`.

You may need to edit `check.sh` so that it checks the correct file path
to your job output. As-is, it assumes that the job output file is at
`$WORK/node_logs/maverick2_JOBID.out`, where (as before) `JOBID` is the 6-digit
ID for your running job.

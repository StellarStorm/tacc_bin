#!/bin/bash
# Check on the output and progress of a running/completed job
# Use like 'check XXXXXX' where XXXXXX is the job ID (reported by 'sbatch'
# when submitting a job, and also by 'squeue -l' at any time during training)
#
# This assumes your .job file writes output to $WORK/node_logs/maverick2_%j.out
# i.e.
#SBATCH -o /work/07300/sgay/maverick2/node_logs/maverick2_%j.out
#SBATCH -e /work/07300/sgay/maverick2/node_logs/maverick2_%j.out

cat $WORK/node_logs/maverick2_$1.out
echo -e "\n"

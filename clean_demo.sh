#!/bin/bash
# Cleanup files created by prototyping. This assumes that model and tensorboard
# logs are saved to $WORK/logs/, weights are saved to $WORK/weights, and
# model jsons are saved to $WORK/json_models
ARCHIVE_DIR=$WORK/archive

rm -rf $WORK/logs/DEMO*
rm -rf $WORK/weights/DEMO*
rm -rf $WORK/json_models/DEMO*
rm -rf $ARCHIVE_DIR/logs/DEMO*
rm -rf $ARCHIVE_DIR/weights/DEMO*
